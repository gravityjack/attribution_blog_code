import os
import csv
import glob
import shutil
import random
import argparse

import keras
import numpy as np
import PIL.Image as Image
#import scipy.ndimage as ndimage
import imageio

def get_input_images(dataset_dir, image_file_type):
    filenames = glob.glob(f'{dataset_dir}/**/*{image_file_type}', recursive=True)
    # the ground truthes are stored by image file number
    filenames.sort()

    images = [open_image(filename) for filename in filenames]
    return np.array(images)

def open_image(filename):
    im = np.array(Image.open(filename).convert('L'))

    width, height = im.shape[0], im.shape[0]

    im = im.reshape(width, height, 1) # add the extra channel
    im = im / 255 # better normalize!  
    return im


def load_ground_truth(dataset_dir, ground_truth_filename):
    filename = os.path.join(dataset_dir, ground_truth_filename)
    truth = np.genfromtxt(filename, delimiter=',')
    truth = keras.utils.to_categorical(truth, 4) # Convert the 1 or 0 into a one hot vector
    return truth;
 
